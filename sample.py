# -*- coding: utf-8 -*-
"""
Created on 2019/3/18 9:51

@author: Ji
"""


class A:
    def __init__(self):
        self.s = 'hello'

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        print('del')


def main():
    with A() as a:
        while True:
            print(a.s)


if __name__ == '__main__':
    main()
