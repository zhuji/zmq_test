# -*- coding: utf-8 -*-
"""
Created on 3/25/19 9:19 AM

@author: Ji
"""

import threading
import time
import zmq
from zmq.eventloop.ioloop import IOLoop
from zmq.eventloop.zmqstream import ZMQStream

NBR_CLIENTS = 10
NBR_WORKERS = 3


def client_thread(_url, _i, _ctx=None):
    """ Basic request-reply client using REQ socket """
    _ctx = _ctx or zmq.Context()
    with _ctx.socket(zmq.REQ) as s:
        s.identity = (u"Client-%d" % _i).encode('ascii')
        s.connect(_url)

        #  Send request, get reply
        s.send(b"HELLO")
        reply = s.recv()
        print("%s: %s\n" % (s.identity.decode('ascii'), reply.decode('ascii')), end='')


def worker_thread(_url, _i, _ctx=None):
    """ Worker using REQ socket to do LRU routing """
    _ctx = _ctx or zmq.Context()
    with _ctx.socket(zmq.REQ) as s:
        s.identity = (u"Worker-%d" % _i).encode('ascii')
        s.connect(_url)

        try:
            # Tell the broker we are ready for work
            s.send(b"READY")
            while True:
                address, empty, request = s.recv_multipart()
                print("%s: %s\n" % (s.identity.decode('ascii'), request.decode('ascii')), end='')
                s.send_multipart([address, b'', b'OK'])
        except zmq.ContextTerminated:
            # context terminated so quit silently
            return


class LRUQueue:
    """LRUQueue class using ZMQStream/IOLoop for event dispatching"""

    def __init__(self, _backend, _frontend):
        self.available_workers = 0
        self.workers = []
        self.client_nbr = NBR_CLIENTS

        self.backend = ZMQStream(_backend)
        self.frontend = ZMQStream(_frontend)
        self.backend.on_recv(self.handle_backend)

        self.loop = IOLoop.instance()

    def handle_backend(self, msg):
        # Queue worker address for LRU routing
        worker_addr, empty, client_addr = msg[:3]

        assert self.available_workers < NBR_WORKERS

        # add worker back to the list of workers
        self.available_workers += 1
        self.workers.append(worker_addr)

        #   Second frame is empty
        assert empty == b""

        # Third frame is READY or else a client reply address
        # If client reply, send rest back to frontend
        if client_addr != b"READY":
            empty, reply = msg[3:]

            # Following frame is empty
            assert empty == b""

            self.frontend.send_multipart([client_addr, b'', reply])

            self.client_nbr -= 1

            if self.client_nbr == 0:
                # Exit after N messages
                self.loop.add_timeout(time.time() + 1, self.loop.stop)

        if self.available_workers == 1:
            # on first recv, start accepting frontend messages
            self.frontend.on_recv(self.handle_frontend)

    def handle_frontend(self, msg):
        # Now get next client request, route to LRU worker
        # Client request is [address][empty][request]
        client_addr, empty, request = msg

        assert empty == b""

        #  Dequeue and drop the next worker address
        self.available_workers -= 1
        worker_id = self.workers.pop()

        self.backend.send_multipart([worker_id, b'', client_addr, b'', request])
        if self.available_workers == 0:
            # stop receiving until workers become available again
            self.frontend.stop_on_recv()


def main():
    """main method"""

    url_worker = "ipc://backend.ipc"
    url_client = "ipc://frontend.ipc"
    # url_worker = 'inproc://backend'
    # url_client = 'inproc://frontend'

    # Prepare our context and sockets
    ctx = zmq.Context()
    frontend = ctx.socket(zmq.ROUTER)
    frontend.bind(url_client)
    backend = ctx.socket(zmq.ROUTER)
    backend.bind(url_worker)

    # create workers and clients threads
    for i in range(NBR_WORKERS):
        thread_w = threading.Thread(target=worker_thread, args=(url_worker, i))
        thread_w.daemon = True
        thread_w.start()

    for i in range(NBR_CLIENTS):
        thread_c = threading.Thread(target=client_thread, args=(url_client, i))
        thread_c.daemon = True
        thread_c.start()

    # create queue with the sockets
    LRUQueue(backend, frontend)

    # start reactor
    IOLoop.instance().start()


if __name__ == "__main__":
    main()
