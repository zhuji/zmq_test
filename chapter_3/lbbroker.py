# -*- coding: utf-8 -*-
"""
Created on 19-3-24 下午2:04

@author: Ji
"""

import multiprocessing
import zmq

NBR_CLIENTS = 10
NBR_WORKERS = 3


def client_task(_url, _ident, _ctx=None):
    """Basic request-reply client using REQ socket."""
    _ctx = _ctx or zmq.Context()
    with _ctx.socket(zmq.REQ) as s:
        s.identity = u"Client-{}".format(_ident).encode("ascii")
        s.connect(_url)

        # Send request, get reply
        s.send(b"HELLO")
        reply = s.recv()
        print("{}: {}".format(s.identity.decode("ascii"),
                              reply.decode("ascii")))


def worker_task(_url, _ident, _ctx=None):
    """Worker task, using a REQ socket to do load-balancing."""
    _ctx = _ctx or zmq.Context()
    with _ctx.socket(zmq.REQ) as s:
        s.identity = u"Worker-{}".format(_ident).encode("ascii")
        s.connect(_url)

        # Tell broker we're ready for work
        s.send(b"READY")

        while True:
            address, empty, request = s.recv_multipart()
            print("{}: {}".format(s.identity.decode("ascii"),
                                  request.decode("ascii")))
            s.send_multipart([address, b"", b"OK"])


def main():
    """Load balancer main loop."""

    url_worker = "ipc://backend.ipc"
    url_client = "ipc://frontend.ipc"
    # url_worker = 'inproc://backend'
    # url_client = 'inproc://frontend'

    # Prepare context and sockets
    ctx = zmq.Context.instance()
    frontend = ctx.socket(zmq.ROUTER)
    frontend.bind(url_client)
    backend = ctx.socket(zmq.ROUTER)
    backend.bind(url_worker)

    # Start background tasks
    def start(task, *args):
        process = multiprocessing.Process(target=task, args=args)
        process.daemon = True
        process.start()

    for i in range(NBR_CLIENTS):
        start(client_task, url_client, i)
    for i in range(NBR_WORKERS):
        start(worker_task, url_worker, i)

    # Initialize main loop state
    count = NBR_CLIENTS
    workers = []
    poller = zmq.Poller()
    # Only poll for requests from backend until workers are available
    poller.register(backend, zmq.POLLIN)

    while True:
        sockets = dict(poller.poll())

        if backend in sockets:
            # Handle worker activity on the backend
            request = backend.recv_multipart()
            worker, empty, client = request[:3]
            if not workers:
                # Poll for clients now that a worker is available
                poller.register(frontend, zmq.POLLIN)
            workers.append(worker)
            if client != b"READY" and len(request) > 3:
                # If client reply, send rest back to frontend
                empty, reply = request[3:]
                frontend.send_multipart([client, b"", reply])
                count -= 1
                if not count:
                    break

        if frontend in sockets:
            # Get next client request, route to last-used worker
            client, empty, request = frontend.recv_multipart()
            worker = workers.pop(0)
            backend.send_multipart([worker, b"", client, b"", request])
            if not workers:
                # Don't poll clients if no workers are available
                poller.unregister(frontend)

    # Clean up
    backend.close()
    frontend.close()
    ctx.term()


if __name__ == "__main__":
    main()
