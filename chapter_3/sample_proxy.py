# -*- coding: utf-8 -*-
"""
Created on 2019/3/22 17:05

@author: Ji
"""

import zmq


def swap(msg):
    if msg == b'\x00\x80\x00\x00)':
        return b'\x00\x80\x00\x00*'
    elif msg == b'\x00\x80\x00\x00*':
        return b'\x00\x80\x00\x00)'
    else:
        raise ValueError


ctx = zmq.Context()
frontend = ctx.socket(zmq.ROUTER)
frontend.bind('tcp://*:5555')
backend = ctx.socket(zmq.DEALER)
backend.bind('tcp://*:5556')
poller = zmq.Poller()
poller.register(frontend, zmq.POLLIN)
poller.register(backend, zmq.POLLIN)

print('start proxy')
while True:
    try:
        # zmq.proxy(frontend, backend)
        socks = dict(poller.poll())
        if socks.get(frontend) == zmq.POLLIN:
            message = frontend.recv_multipart()
            backend.send_multipart(message)

        if socks.get(backend) == zmq.POLLIN:
            message = backend.recv_multipart()
            # message[0] = swap(message[0])
            frontend.send_multipart(message)

    except KeyboardInterrupt:
        print('interrupted')
        break
