# -*- coding: utf-8 -*-
"""
Created on 2019/3/22 17:04

@author: Ji
"""

import zmq
import sys

ctx = zmq.Context()
s = ctx.socket(zmq.REP)
s.connect('tcp://localhost:5556')
address = 'end'.encode()

print('start end with address', address)

while True:
    try:
        message = s.recv()
        s.send(b'reply for ' + message + b' from ' + address)
    except KeyboardInterrupt:
        print('interrupted')
        break
