# -*- coding: utf-8 -*-
"""
Created on 19-3-24 下午1:17

@author: Ji
"""

import time
import random
from threading import Thread
import zmq
import zhelpers

NBR_WORKERS = 10


def worker_thread(_ctx):
    with _ctx.socket(zmq.REQ) as s:

        # We use a string identity for ease here
        zhelpers.set_id(s)
        s.connect("tcp://localhost:5671")

        total = 0
        while True:
            # Tell the router we're ready for work
            s.send(b"ready")

            # Get workload from router, until finished
            if s.recv() == b"END":
                print("Processed: %d tasks" % total)
                break
            total += 1

            # Do some random work
            time.sleep(0.1 * random.random())


ctx = zmq.Context.instance()
client = ctx.socket(zmq.ROUTER)
client.bind("tcp://*:5671")

for _ in range(NBR_WORKERS):
    Thread(target=worker_thread, args=(ctx,)).start()

for _ in range(NBR_WORKERS * 10):
    # LRU worker is next waiting in the queue
    address, empty, ready = client.recv_multipart()

    client.send_multipart([
        address,
        b'',
        b'This is the workload',
    ])

# Now ask mama to shut down and report their results
for _ in range(NBR_WORKERS):
    address, empty, ready = client.recv_multipart()
    client.send_multipart([
        address,
        b'',
        b'END',
    ])
