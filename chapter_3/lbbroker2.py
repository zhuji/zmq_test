# -*- coding: utf-8 -*-
"""
Created on 3/25/19 9:07 AM

@author: Ji
"""

import threading
import time
import zmq

NBR_CLIENTS = 10
NBR_WORKERS = 3


def client_thread(_url, _i, _ctx=None):
    """ Basic request-reply client using REQ socket """

    _ctx = _ctx or zmq.Context()
    with _ctx.socket(zmq.REQ) as s:
        # Set client identity. Makes tracing easier
        s.identity = (u"Client-%d" % _i).encode('ascii')
        s.connect(_url)

        #  Send request, get reply
        s.send(b"HELLO")
        reply = s.recv()
        print("%s: %s\n" % (s.identity.decode('ascii'), reply.decode('ascii')), end='')


def worker_thread(_url, _i, _ctx=None):
    """ Worker using REQ socket to do LRU routing """

    _ctx = _ctx or zmq.Context()
    with _ctx.socket(zmq.REQ) as s:
        # set worker identity
        s.identity = (u"Worker-%d" % _i).encode('ascii')
        s.connect(_url)

        # Tell the broker we are ready for work
        s.send(b"READY")

        try:
            while True:
                address, empty, request = s.recv_multipart()
                print("%s: %s\n" % (s.identity.decode('ascii'), request.decode('ascii')), end='')
                s.send_multipart([address, b'', b'OK'])
        except zmq.ContextTerminated:
            # context terminated so quit silently
            raise zmq.ContextTerminated


def main():
    """ main method """

    url_worker = "inproc://workers"
    url_client = "inproc://clients"
    client_nbr = NBR_CLIENTS

    # Prepare our context and sockets
    ctx = zmq.Context()
    frontend = ctx.socket(zmq.ROUTER)
    frontend.bind(url_client)
    backend = ctx.socket(zmq.ROUTER)
    backend.bind(url_worker)

    # create workers and clients threads
    for i in range(NBR_WORKERS):
        thread_w = threading.Thread(target=worker_thread, args=(url_worker, i, ctx))
        thread_w.start()

    for i in range(NBR_CLIENTS):
        thread_c = threading.Thread(target=client_thread, args=(url_client, i, ctx))
        thread_c.start()

    # Logic of LRU loop
    # - Poll backend always, frontend only if 1+ worker ready
    # - If worker replies, queue worker as ready and forward reply
    # to client if necessary
    # - If client requests, pop next worker and send request to it

    # Queue of available workers
    available_workers = 0
    workers_list = []

    # init poller
    poller = zmq.Poller()

    # Always poll for worker activity on backend
    poller.register(backend, zmq.POLLIN)

    # Poll front-end only if we have available workers
    poller.register(frontend, zmq.POLLIN)

    while True:

        socks = dict(poller.poll())

        # Handle worker activity on backend
        if backend in socks and socks[backend] == zmq.POLLIN:

            # Queue worker address for LRU routing
            message = backend.recv_multipart()
            assert available_workers < NBR_WORKERS

            worker_addr = message[0]

            # add worker back to the list of workers
            available_workers += 1
            workers_list.append(worker_addr)

            #   Second frame is empty
            empty = message[1]
            assert empty == b""

            # Third frame is READY or else a client reply address
            client_addr = message[2]

            # If client reply, send rest back to frontend
            if client_addr != b'READY':

                # Following frame is empty
                empty = message[3]
                assert empty == b""

                reply = message[4]

                frontend.send_multipart([client_addr, b"", reply])

                client_nbr -= 1

                if client_nbr == 0:
                    break  # Exit after N messages

        # poll on frontend only if workers are available
        if available_workers > 0:

            if frontend in socks and socks[frontend] == zmq.POLLIN:
                # Now get next client request, route to LRU worker
                # Client request is [address][empty][request]

                [client_addr, empty, request] = frontend.recv_multipart()

                assert empty == b""

                #  Dequeue and drop the next worker address
                available_workers += -1
                worker_id = workers_list.pop()

                backend.send_multipart([worker_id, b"",
                                        client_addr, b"", request])

    # out of infinite loop: do some housekeeping
    time.sleep(1)

    frontend.close()
    backend.close()
    ctx.term()


if __name__ == "__main__":
    main()
