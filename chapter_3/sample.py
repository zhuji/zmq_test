# -*- coding: utf-8 -*-
"""
Created on 19-3-24 下午1:46

@author: Ji
"""

import zmq

ctx = zmq.Context()
s = ctx.socket(zmq.REQ)
s.setsockopt(zmq.IDENTITY, b's')
s.setsockopt(zmq.REQ_CORRELATE, 1)
s.setsockopt(zmq.SNDTIMEO, 1000)
s.connect('inproc://sample')

c = ctx.socket(zmq.ROUTER)
c.identity = b'c'
c.bind('inproc://sample')

s.send(b'hello')
msg = c.recv_multipart()
print(msg)
