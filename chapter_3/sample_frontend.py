# -*- coding: utf-8 -*-
"""
Created on 2019/3/22 17:04

@author: Ji
"""

import zmq
import sys

ctx = zmq.Context()
s = ctx.socket(zmq.REQ)
s.connect('tcp://localhost:5555')
address = sys.argv[1].encode()

print('start front with address', address)

try:
    s.send(b'sending from ' + address)
    print(s.recv())
except KeyboardInterrupt:
    print('interrupted')
