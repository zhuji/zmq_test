# -*- coding: utf-8 -*-
"""
Created on 2019/3/14 18:39

@author: Ji
"""

import zmq
import time
from util.configs import *

ctx = zmq.Context.instance()
with ctx.socket(zmq.PAIR) as s:
    s.set_string(zmq.IDENTITY, 'server')
    s.bind('{}:{}'.format(CLIENT_IP, PORT))
    i = 0

    while True:
        s.send_string('server msg{} to {}'.format(i, CLIENT_IP))
        print('already sent')
        i += 1
        msg = s.recv_string()
        print(msg)
        msg = s.recv_string()
        print(msg)
        time.sleep(1)
