# -*- coding: utf-8 -*-
"""
Created on 2019/3/14 19:51

@author: Ji
"""

import zmq
from util.configs import *

ctx = zmq.Context.instance()
with ctx.socket(zmq.PAIR) as s:
    s.set_string(zmq.IDENTITY, 'client')
    s.connect('{}:{}'.format(SERVER_IP, PORT))
    i = 0

    while True:
        msg = s.recv_string()
        print(msg)
        s.send_string('client msg[a]{} to {}'.format(i, SERVER_IP))
        print('already sent')
        s.send_string('client msg[b]{} to {}'.format(i, SERVER_IP))
        print('already sent')
        i += 1
