# -*- coding: utf-8 -*-
"""
Created on 3/28/19 6:18 AM

@author: Ji
"""

import asyncio
import random
import zmq.asyncio

context = zmq.asyncio.Context()
socket = context.socket(zmq.PUSH)
socket.setsockopt(zmq)
socket.connect('tcp://localhost:5555')


async def main():
    while True:
        msg = str(random.randint(1, 100)).encode()
        print(msg)
        await socket.send(msg)


loop = asyncio.get_event_loop()
asyncio.ensure_future(main())
loop.run_forever()
