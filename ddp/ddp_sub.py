# -*- coding: utf-8 -*-
"""
Created on 2019/3/20 19:51

@author: Ji
"""

import zmq
import sys

ctx = zmq.Context()
s = ctx.socket(zmq.SUB)
s.connect('tcp://localhost:5556')
address = sys.argv[1].encode()
s.subscribe(address)

print('start sub subscribing address', address)

while True:
    try:
        address, message = s.recv_multipart(zmq.NOBLOCK)
        print(message)
        break
    except zmq.Again:
        continue
    except:
        break
