# -*- coding: utf-8 -*-
"""
Created on 2019/3/20 19:42

@author: Ji
"""

import zmq
import sys

ctx = zmq.Context()
s = ctx.socket(zmq.PUB)
s.connect('tcp://localhost:5555')
address = sys.argv[1].encode()
message = b'hello from ' + address

print('start pub with address', address)

while True:
    try:
        s.send_multipart([address, message])
    except KeyboardInterrupt:
        print('interrupted')
        break
