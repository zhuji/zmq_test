# -*- coding: utf-8 -*-
"""
Created on 2019/3/20 19:48

@author: Ji
"""

import zmq

ctx = zmq.Context()
s = ctx.socket(zmq.XSUB)
s.bind('tcp://*:5555')
c = ctx.socket(zmq.XPUB)
c.bind('tcp://*:5556')

print('start proxy')

while True:
    try:
        zmq.proxy(s, c)
    except KeyboardInterrupt:
        print('interrupted')
        break
