# -*- coding: utf-8 -*-
"""
Created on 4/16/19 3:46 PM

@author: Ji
"""

import zmq

ctx = zmq.Context()
s = ctx.socket(zmq.ROUTER)
s.setsockopt(zmq.IDENTITY, b'you')
s.setsockopt(zmq.ROUTER_HANDOVER, 1)
s.bind('tcp://*:5555')

print(s.recv_multipart())
