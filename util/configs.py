# -*- coding: utf-8 -*-
"""
Created on 2019/3/14 19:20

@author: Ji
"""

import os
import configparser

config = configparser.ConfigParser()
config.read(os.path.dirname(os.path.realpath(__file__)) + '/config.conf')

PORT = config['addr']['port']
CLIENT_IP = config['addr']['client_ip']
SERVER_IP = config['addr']['server_ip']
ANY_IP = config['addr']['any_ip']
