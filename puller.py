# -*- coding: utf-8 -*-
"""
Created on 3/28/19 6:18 AM

@author: Ji
"""

import asyncio
import zmq.asyncio

context = zmq.asyncio.Context()
socket = context.socket(zmq.PULL)
socket.bind('tcp://*:5555')


async def main():
    while True:
        print(await socket.recv_multipart())


loop = asyncio.get_event_loop()
asyncio.ensure_future(main())
loop.run_forever()
