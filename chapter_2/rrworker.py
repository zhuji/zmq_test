# -*- coding: utf-8 -*-
"""
Created on 2019/3/19 15:20

@author: Ji
"""

import zmq

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.connect("tcp://localhost:5560")

while True:
    message = socket.recv()
    print("Received request: %s" % message)
    socket.send(b"World")
