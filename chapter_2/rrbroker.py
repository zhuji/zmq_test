# -*- coding: utf-8 -*-
"""
Created on 2019/3/19 15:21

@author: Ji
"""

import zmq

# Prepare our context and sockets
context = zmq.Context()
frontend = context.socket(zmq.ROUTER)
backend = context.socket(zmq.DEALER)
frontend.bind("tcp://*:5559")
backend.bind("tcp://*:5560")


def main_broker():
    # Initialize poll set
    poller = zmq.Poller()
    poller.register(frontend, zmq.POLLIN)
    poller.register(backend, zmq.POLLIN)

    # Switch messages between sockets
    while True:
        socks = dict(poller.poll())

        # 必须使用multipart来接受和发送
        if socks.get(frontend) == zmq.POLLIN:
            message = frontend.recv_multipart()
            backend.send_multipart(message)

        if socks.get(backend) == zmq.POLLIN:
            message = backend.recv_multipart()
            frontend.send_multipart(message)


def main_proxy():
    zmq.proxy(frontend, backend)


if __name__ == '__main__':
    main_proxy()
    # We never get here…
    frontend.close()
    backend.close()
    context.term()
