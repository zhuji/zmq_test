# -*- coding: utf-8 -*-
"""
Created on 2019/3/18 17:40

@author: Ji
"""

import zmq

# Prepare our context and sockets
context = zmq.Context()

# Connect to task ventilator
receiver = context.socket(zmq.PULL)
receiver.connect("tcp://localhost:5557")

# Connect to weather server
subscriber = context.socket(zmq.SUB)
subscriber.connect("tcp://localhost:5556")
subscriber.setsockopt(zmq.SUBSCRIBE, b"10001")

# Initialize poll set
poller = zmq.Poller()
poller.register(receiver, zmq.POLLIN)
poller.register(subscriber, zmq.POLLIN)

# Process messages from both sockets
while True:
    try:
        socks = dict(poller.poll())  # blocks until there's data on either socket
    except KeyboardInterrupt:
        break

    if receiver in socks:
        message = receiver.recv(zmq.NOBLOCK)
        # process task

    if subscriber in socks:
        message = subscriber.recv(zmq.DONTWAIT)
        # process weather update
