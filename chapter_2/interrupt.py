# -*- coding: utf-8 -*-
"""
Created on 2019/3/19 21:46

@author: Ji
"""

# Shows how to handle Ctrl-C

import zmq

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5558")

# SIGINT will normally raise a KeyboardInterrupt, just like any other Python call
try:
    socket.recv()
except KeyboardInterrupt:
    print("W: interrupt received, stopping…")
finally:
    # clean up
    socket.close()
    context.term()
