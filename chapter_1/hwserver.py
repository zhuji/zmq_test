# -*- coding: utf-8 -*-
"""
Created on 2019/3/17 14:18

@author: Ji
"""

import time
import zmq

context = zmq.Context()
socket = context.socket(zmq.REP)  # REP模式下必须先接收再发送
socket.bind("tcp://*:5555")

while True:
    #  Wait for next request from client
    message = socket.recv()
    print("Received request: %s" % message)

    #  Do some 'work'
    time.sleep(1)

    #  Send reply back to client
    socket.send(b"World")
    print("sent already!")
