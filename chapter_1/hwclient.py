# -*- coding: utf-8 -*-
"""
Created on 2019/3/17 14:18

@author: Ji
"""

import zmq

context = zmq.Context()

#  Socket to talk to server
print("Connecting to hello world server…")
socket = context.socket(zmq.REQ)  # REQ模式下必须先发送再接收
socket.connect("tcp://localhost:5555")

#  Do 10 requests, waiting each time for a response
for request in range(10):
    print("Sending request %s …" % request)
    socket.send(b"Hello")  # 非阻塞

    #  Get the reply.
    message = socket.recv()
    print("Received reply %s [ %s ]" % (request, message))
